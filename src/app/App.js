import classes from './App.module.scss';
import City from "./Component/City/City";
import Weather from "./Component/Weather/Weather";
import {useState} from "react";
import axios from 'axios';


function App() {
    const API_key="137a8dc900a266046a122c55842da2d4"
    const [city, setCity] = useState()
    const [weather, setWeather] = useState()
    const fetchWeather = async (e) => {
        e.preventDefault()
        const res = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_key}`)
        if (res.status === 200) {
            console.log(res.data)
            setWeather(res.data)
        }
    }
    return (
        <div className={classes.App}>
            <div className={classes.Container}>
                <h1 className={classes.Title}>React Weather App</h1>
                {
                    weather ? <Weather weather={weather} /> : <City setCity={setCity} fetchWeather={fetchWeather}/>
                }
            </div>
        </div>
    );
}

export default App;
