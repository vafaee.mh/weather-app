import classes from './Weather.module.scss'
import {BsDropletHalf, CgCompressLeft, FaCloudShowersHeavy, FiSun, GiWhirlwind} from "react-icons/all";
import {
    brokenClouds,
    clearSky1,
    clearSky2,
    fewClouds1,
    fewClouds2, mist, rain1,
    rain2,
    scatteredClouds,
    showerRain, snow, thunderstorm
} from "../../File/img";

const weatherIcon = {
    "01d": clearSky1,
    "01n": clearSky2,
    "02d": fewClouds1,
    "02n": fewClouds2,
    "03d":scatteredClouds,
    "03n":scatteredClouds,
    "04n":brokenClouds,
    "04d": brokenClouds,
    "09n": showerRain,
    "09d": showerRain,
    "10n": rain1,
    "10d": rain2,
    "11n": thunderstorm,
    "11d": thunderstorm,
    "13n": snow,
    "13d": snow,
    "50n": mist,
    "50d": mist
}
export default function Weather({weather}) {
    const isDay = weather.weather[0].icon.includes('d');
    const getTime = (time) => {
        return `${new Date(time * 1000).getHours()}:${new Date(time * 1000).getMinutes()}`
    }

    console.log(weather.weather[0].icon)
    return (
        <div className={classes.Weather}>
            <div className={classes.Container}>
                <span>{`${Math.floor(weather.main.temp - 273)}°C|${weather.weather[0].description}`}</span>
                <img className={classes.Image} src={weatherIcon[weather?.weather[0].icon]} alt=""/>

            </div>
            <h1 className={classes.Title}>{`${weather?.name},${weather?.sys?.country}`}</h1>
            <h4 className={classes.TitleInfo}>WeatherInfo</h4>
            <div className={'row'}>
                <div className={classes.Part}>
                    <div className={'col-md-6'}>
                        <div className={classes.AlignBoxIcon}>
                            <span className={classes.Icon}><FiSun/></span>
                            <div className={classes.AlignDetails}>
                                <span>{getTime(weather.sys[isDay ? "sunset" : "sunrise"])}</span>
                                <span className={classes.Text}>{isDay ? "غروب خورشید" : "طلوع خورشید"}</span>
                            </div>
                        </div>
                    </div>
                    <div className={'col-md-6'}>
                        <div className={classes.AlignBoxIcon}>
                            <span className={classes.Icon}><BsDropletHalf/></span>
                            <div className={classes.AlignDetails}>
                                <span>{weather.main.humidity}</span>
                                <span className={classes.Text}>رطوبت</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={classes.Part}>
                    <div className={'col-md-6'}>
                        <div className={classes.AlignBoxIcon}>
                            <span className={classes.Icon}><GiWhirlwind/></span>
                            <div className={classes.AlignDetails}>
                                <span>{weather.wind.speed}  </span>
                                <span className={classes.Text}>باد</span>
                            </div>
                        </div>
                    </div>
                    <div className={'col-md-6'}>
                        <div className={classes.AlignBoxIcon}>
                            <span className={classes.Icon}><CgCompressLeft/></span>
                            <div className={classes.AlignDetails}>
                                <span>{weather.main.pressure} </span>
                                <span className={classes.Text}>فشار</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}
