import classes from './City.module.scss'
import {FaCloudShowersHeavy} from "react-icons/all";
import {Button} from "@material-ui/core";


export default function City({setCity,fetchWeather}) {

    return (
        <div className={classes.City}>
            <span className={classes.Icon}>
                    <FaCloudShowersHeavy/>
            </span>
            <h2 className={classes.subTitle}>Find weather OF Your City</h2>
            <div className={classes.BoxInput}>
                <input className={classes.Input} type="text" placeholder={'Your City'}
                       onChange={(e)=>{setCity(e.target.value)}}/>
                <Button  onClick={fetchWeather} className={classes.Button} variant="contained" >
                    Search
                </Button>
            </div>
        </div>
    )
}